var kinectron = null;
var myImage, frame;

var rightHandX; //have to mention them here 
var rightHandY; //because they are shared between draw and trackBody
let numBalls = 15;
let spring = 0.05;
let gravity = 0.03;
let friction = -0.9;
let balls = [];

function preload() {
  myImage = loadImage("http://molleindustria.github.io/p5.play/examples/assets/asterisk_circle0008.png");
  frame = loadImage('http://molleindustria.github.io/p5.play/examples/assets/asterisk_stretching0008.png');
  enemy= loadImage('http://molleindustria.github.io/p5.play/examples/assets/ghost_standing0001.png');
}

function fantasma(){
  this.x = windowWidth/2;
  this.y = windowHeight/2;

  this.show  = function (){
    image(myImage, this.x, this.y, 100, 100);
    if (myImage === frame) {
      noLoop();
    }
  }

  this.update = function(){
    this.x = rightHandX;
    this.y = rightHandY;
  }

  this.morir = function(){
    myImage = frame;
  }

}

class Ball {
  constructor(xin, yin, din, idin, oin) {
    this.x = xin;
    this.y = yin;
    this.vx = random(0,3);
    this.vy = random(0,3);
    this.diameter = din;
    this.id = idin;
    this.others = oin;

  }

  collide() {
    for (let i = this.id + 1; i < numBalls; i++) {
      // console.log(others[i]);
      let dx = this.others[i].x - this.x;
      let dy = this.others[i].y - this.y;
      let distance = sqrt(dx * dx + dy * dy);
      let minDist = this.others[i].diameter / 2 + this.diameter / 2;
      //   console.log(distance);
      //console.log(minDist);
      if (distance < minDist) {
        //console.log("2");
        let angle = atan2(dy, dx);
        let targetX = this.x + cos(angle) * minDist;
        let targetY = this.y + sin(angle) * minDist;
        let ax = (targetX - this.others[i].x) * spring;
        let ay = (targetY - this.others[i].y) * spring;
        this.vx -= ax;
        this.vy -= ay;
        this.others[i].vx += ax;
        this.others[i].vy += ay;
      }
    }
  }

  kill(){
    for (let i = this.id + 1; i < numBalls; i++) {
      // console.log(others[i]);
      let dx = fantasma.x - this.x;
      let dy = fantasma.y - this.y;
      let distance = sqrt(dx * dx + dy * dy);
      let minDist = 10 / 2 + this.diameter;
      //   console.log(distance);
      //console.log(minDist);
      if (distance < minDist) {
        //console.log("2");
        fantasma.morir();
      }
    }
  }

  move() {
    this.x += this.vx;
    this.y += this.vy;
    if (this.x + this.diameter / 2 > windowWidth) {
      this.x = windowWidth - this.diameter / 2;
      this.vx *= friction;
    } else if (this.x - this.diameter / 2 < 0) {
      this.x = this.diameter / 2;
      this.vx *= friction;
    }
    if (this.y + this.diameter / 2 > height) {
      this.y = height - this.diameter / 2;
      this.vy *= friction;
    } else if (this.y - this.diameter / 2 < 0) {
      this.y = this.diameter / 2;
      this.vy *= friction;
    }
  }

  display() {
    image(enemy,this.x, this.y, this.diameter, this.diameter);
  }
}



function setup() {
  createCanvas(windowWidth, windowHeight);
  var address = {
    host: '192.168.121.1',
    port: 9001,
    path: '/'
  };
  kinectron = new Kinectron('kinectron', address);
  kinectron.makeConnection();
  kinectron.startTrackedBodies(trackBody);
  fantasma = new fantasma();
  for (let i = 0; i < numBalls; i++) {
    balls[i] = new Ball(
      random(windowWidth),
      random(windowHeight),
      random(50, 50),
      i,
      balls
    );
  }
  //console.log(kinectron.makeConnection());
  //set the callback function name to be called when stuff comes from kinect
}

function draw() {
  background(25);
  //image(frame, 0, 0);
  fantasma.show();
  fantasma.update();
  balls.forEach(ball => {
    ball.collide();
    ball.move();
    ball.display();
    ball.kill();
  });

}

function trackBody(body) {
  if (body.rightHandState == 3) { //look in console at the bottom for users. 
    //only works if for a particular user with hand in a fist.
    var val;
    val = body.joints[kinectron.HANDRIGHT].depthX;
    rightHandX = map(val, 0, 1, 0, width);
    val = body.joints[kinectron.HANDRIGHT].depthY;
    rightHandY = map(val, 0, 1, 0, height); //height numbers bigger at the bottom
  }
  //console.log(body.bodyIndex + " " + body.leftHandState + "  " + body.rightHandState + " " + val);

}